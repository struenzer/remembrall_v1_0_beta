namespace :remembrall do
  desc "TODO" #todo add desc
  task :enqueue_reminder => :environment do
    occurences = EventOccurrence.week
    occurences.each do |occ|
      if occ.schedulable.remember?
        # check if job doesn't exists already
        telegram_id = occ.schedulable.user.telegram_id
        username = occ.schedulable.user.username
        remind_text = occ.schedulable.name
        remind_date = occ.date.strformat('%H:%M Uhr')
        SendRemindWorker.perform_at(occ.date, occ.id)
        end
      #ReminderManager.enqueue(occ)
    end
  end
  task :enqueue => :environment do
    occurrence = EventOccurrence.week.first
    SendRemindWorker.perform_in(2.minutes, occurrence.id )
    #SendRemind.perform_in(5.minutes).remind(occurrence)
    #RemindJob.delay(:run_at => occurrence.date).perform(occurrence)
  end
end
