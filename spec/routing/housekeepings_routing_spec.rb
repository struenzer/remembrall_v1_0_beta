require "rails_helper"

RSpec.describe HousekeepingsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/housekeepings").to route_to("housekeepings#index")
    end

    it "routes to #new" do
      expect(:get => "/housekeepings/new").to route_to("housekeepings#new")
    end

    it "routes to #show" do
      expect(:get => "/housekeepings/1").to route_to("housekeepings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/housekeepings/1/edit").to route_to("housekeepings#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/housekeepings").to route_to("housekeepings#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/housekeepings/1").to route_to("housekeepings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/housekeepings/1").to route_to("housekeepings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/housekeepings/1").to route_to("housekeepings#destroy", :id => "1")
    end

  end
end
