class Housekeeping < ApplicationRecord
  acts_as_schedulable :schedule, occurrences: :event_occurrences

  belongs_to :user
end
