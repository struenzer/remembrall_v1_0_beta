require 'rails_helper'

RSpec.describe "housekeepings/index", type: :view do
  before(:each) do
    assign(:housekeepings, [
      Housekeeping.create!(
        :name => "Name",
        :descr => "MyText",
        :remember => false
      ),
      Housekeeping.create!(
        :name => "Name",
        :descr => "MyText",
        :remember => false
      )
    ])
  end

  it "renders a list of housekeepings" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
