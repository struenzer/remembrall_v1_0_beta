class ApplicationMailer < ActionMailer::Base
  default from: 'remembrall@bflmedia.rockt.es'
  layout 'mailer'
end
