class SendRemind
  def self.remind(occurence_id)
    occurence = EventOccurrence.find(occurence_id)

    username = occurence.schedulable.user.username
    telegram_id = occurence.schedulable.user.telegram_id
    remind = occurence.schedulable.name
    date = occurence.date.strftime('')

    SendMessage.send_message telegram_id, text
  end

  def generate_text(username, remind)
    "Hallo #{username}! Es ist jetzt"
  end
end