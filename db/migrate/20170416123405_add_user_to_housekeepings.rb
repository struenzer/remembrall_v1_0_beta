class AddUserToHousekeepings < ActiveRecord::Migration[5.0]
  def change
    add_reference :housekeepings, :user, foreign_key: true
  end
end
