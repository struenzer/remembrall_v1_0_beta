class CreateHousekeepings < ActiveRecord::Migration[5.0]
  def change
    create_table :housekeepings do |t|
      t.string :name
      t.text :descr
      t.boolean :remember

      t.timestamps
    end
  end
end
