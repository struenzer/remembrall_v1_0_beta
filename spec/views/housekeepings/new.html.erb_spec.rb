require 'rails_helper'

RSpec.describe "housekeepings/new", type: :view do
  before(:each) do
    assign(:housekeeping, Housekeeping.new(
      :name => "MyString",
      :descr => "MyText",
      :remember => false
    ))
  end

  it "renders new housekeeping form" do
    render

    assert_select "form[action=?][method=?]", housekeepings_path, "post" do

      assert_select "input#housekeeping_name[name=?]", "housekeeping[name]"

      assert_select "textarea#housekeeping_descr[name=?]", "housekeeping[descr]"

      assert_select "input#housekeeping_remember[name=?]", "housekeeping[remember]"
    end
  end
end
