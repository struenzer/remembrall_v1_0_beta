require 'sidekiq/web'
Rails.application.routes.draw do
  resources :housekeepings
  get 'home/index'
  get 'home/about'

  # declare own devise controller
  devise_for :users, controllers: { registrations: "registrations"}

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index' #TODO needs to be changed
  telegram_webhooks TelegramWebhookController

  # only allow authenticated users to view sidekiq
  authenticate :user do
  mount Sidekiq::Web => '/sidekiq'
  end

end
