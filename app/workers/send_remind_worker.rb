class SendRemindWorker
  include Sidekiq::Worker
  sidekiq_options :retry => 2, unique: :until_and_while_executing
  def perform(id)
    occurrence = EventOccurrence.find(id)
    telegram_id = occurrence.schedulable.user.telegram_id
    user = occurrence.schedulable.user.username
    date = occurrence.date.strftime('%H:%M Uhr')
    text = "Hey #{user}! Es ist jetzt #{date}. Ich möchte dich an folgendes erinnern *#{occurrence.schedulable.name}*."
    SendMessage.send(telegram_id, text)
  end
end