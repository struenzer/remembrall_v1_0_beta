class SendMessage
  def self.send(telegram_id, text)
    bot = Telegram.bot
    bot.send_message chat_id: telegram_id, text: text, parse_mode: 'markdown'
  end
end