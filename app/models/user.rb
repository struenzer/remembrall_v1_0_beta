class User < ApplicationRecord
  # TODO: Add Role Support
  # TODO:
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :housekeepings
  has_many :event_occurrences, through: :housekeepings
end
