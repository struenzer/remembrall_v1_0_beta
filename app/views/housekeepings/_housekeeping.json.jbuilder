json.extract! housekeeping, :id, :name, :descr, :remember, :created_at, :updated_at
json.url housekeeping_url(housekeeping, format: :json)
