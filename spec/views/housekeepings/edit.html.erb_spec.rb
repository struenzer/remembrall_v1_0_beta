require 'rails_helper'

RSpec.describe "housekeepings/edit", type: :view do
  before(:each) do
    @housekeeping = assign(:housekeeping, Housekeeping.create!(
      :name => "MyString",
      :descr => "MyText",
      :remember => false
    ))
  end

  it "renders the edit housekeeping form" do
    render

    assert_select "form[action=?][method=?]", housekeeping_path(@housekeeping), "post" do

      assert_select "input#housekeeping_name[name=?]", "housekeeping[name]"

      assert_select "textarea#housekeeping_descr[name=?]", "housekeeping[descr]"

      assert_select "input#housekeeping_remember[name=?]", "housekeeping[remember]"
    end
  end
end
