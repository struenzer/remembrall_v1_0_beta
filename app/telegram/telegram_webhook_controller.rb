class TelegramWebhookController < Telegram::Bot::UpdatesController
  include Telegram::Bot::UpdatesController::MessageContext
  context_to_action!

  include Telegram::Bot::UpdatesController::Session
  # or just shortcut:
  use_session!

  def housekeeping(value = nil, *)
    telegram_id = from ? from['id'] : nil # assign telegram_id from hat if any or nil
    if is_user_registered? telegram_id # only proceed if user has an account
      if value
        value = value.to_sym
        if value.eql? :today
          today telegram_id
        elsif value.eql? :tomorrow
          tomorrow telegram_id
        elsif value.eql? :week
          week telegram_id
        end
      else
        save_context :housekeeping
        respond_with :message, text: I18n.t('telegram.housekeeping.choose'), reply_markup: {
            keyboard: [%w[today tomorrow week]],
            resize_keyboard: true,
            one_time_keyboard: true,
            selective: true,
        }; Al
      end
    end
  end

  def help(*)
    respond_with :message, text: I18n.t('telegram.help.content')
  end

  def telegramid(*)
    response = from ? I18n.t('telegram.telegramid.response', id: from['id']) : 'Hi there!'
    respond_with :message, text: response
  end

  private

  # returns user_id by telegram_id
  def get_user_id(telegram_id)
    User.where(:telegram_id => telegram_id).pluck(:id).first
  end

  def today(telegram_id)
    user = User.find(get_user_id(telegram_id))
    today = user.event_occurrences.today
    if today.count == 0
      respond_with :message, text: I18n.t('telegram.housekeeping.nothing_today'), parse_mode: 'Markdown'
    else
      respond_each_occurrence(:today, today)
      end
  end

  def tomorrow(telegram_id)
    user = User.find(get_user_id(telegram_id))#tomorrow = EventOccurrence.tomorrow
    tomorrow = user.event_occurrences.tomorrow
    if tomorrow.count == 0
      respond_with :message, text: I18n.t('telegram.housekeeping.nothing_tomorrow'), parse_mode: 'Markdown'
    else
      respond_each_occurrence(:tomorrow, tomorrow)
      end
  end

  def week(telegram_id)
    user = User.find(get_user_id(telegram_id))
    week = user.event_occurrences.week
    if week.count == 0
      respond_with :message, text: I18n.t('telegram.housekeeping.nothing_week'), parse_mode: 'Markdown'
    else
      respond_each_occurrence(:week, week)
    end
  end

    def month(telegram_id)
    user = User.find(get_user_id(telegram_id))
    month = user.event_occurrences.month
    if week.count == 0
      respond_with :message, text: I18n.t('telegram.housekeeping.nothing_month'), parse_mode: 'Markdown'
    else
      respond_each_occurrence(:month, month)
    end
  end

  def is_user_registered?(telegram_id)
    if get_user_id(telegram_id).nil?
      # user not registered
      respond_with :message, text: "Fehler: Du brauchst einen Account auf http://thiago-dev.com."
      false
    else # todo implement user type
      true
    end
  end


  # Responds todos.
  # when_ as symbol and occurrence as EventOccurrence
  def respond_each_occurrence(when_, occurrence)
    case when_
      when :today
        header = "#{I18n.t('telegram.housekeeping.today')}\n"
      when :tomorrow
        header = "#{I18n.t('telegram.housekeeping.tomorrow')}\n"
      when :week
        header = "#{I18n.t('telegram.housekeeping.week')}\n"
	  when :month
        header = "#{I18n.t('telegram.housekeeping.month')}\n"
      else
        header = "#{I18n.t('telegram.housekeeping.default')}"
    end
    body = ''
    occurrence.each do |occ|
      body = body + "*#{occ.schedulable.name}* #{I18n.t('telegram.housekeeping.due_at')} *#{I18n.l(occ.date, format: :short )}*\n"
    end
    respond_with :message, text: "#{header}\n#{body}", parse_mode: 'Markdown'
  end
end