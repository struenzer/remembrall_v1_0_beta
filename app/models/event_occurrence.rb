class EventOccurrence < ActiveRecord::Base
  belongs_to :schedulable, polymorphic: true
  default_scope lambda{order('date ASC')}
  scope :remaining, lambda{where(["date >= ?",Time.now])}
  scope :previous, lambda{where(["date < ?",Time.now])}
  scope :today, lambda{where(:date => Time.now...Time.now.end_of_day)}
  scope :week, lambda{where(:date => Time.now...7.days.from_now)}
  scope :tomorrow, lambda{where(:date => Time.now.tomorrow.beginning_of_day...Time.now.tomorrow.end_of_day)}
  scope :month, lambda{where(:date => Time.now...30.days.from_now)}
  #scope :telegramid, ->(users) { where(users: object.id) }
  #scope :with_telegramid, -> (telegramid) {joins(:users).merge(User.where(telegram_id: telegramid))}
end
