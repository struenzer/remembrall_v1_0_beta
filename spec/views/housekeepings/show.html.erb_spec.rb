require 'rails_helper'

RSpec.describe "housekeepings/show", type: :view do
  before(:each) do
    @housekeeping = assign(:housekeeping, Housekeeping.create!(
      :name => "Name",
      :descr => "MyText",
      :remember => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
