# Be sure to restart your server when you modify this file.

#Rails.application.config.session_store :cookie_store, key: '_Remembrall_session'
#Rails.application.config.session_store :redis_store, servers: ["redis://localhost:6379/0/session"]
Rails.application.config.session_store :active_record_store, :key => '_my_app_session'