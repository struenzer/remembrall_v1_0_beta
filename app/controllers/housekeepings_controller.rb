class HousekeepingsController < ApplicationController
  before_action :set_housekeeping, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  # manage authorization
  #load_and_authorize_resource :only => [:index, :show, :edit, :update, :destroy]
  #skip_authorize_resource :only => [:new, :remember, :create]

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message


  end
  # GET /housekeepings
  # GET /housekeepings.json
  def index
    @housekeepings = current_user.housekeepings
  end

  # GET /housekeepings/1
  # GET /housekeepings/1.json
  def show
    authorize! :show, @housekeeping
  end

  # GET /housekeepings/new
  def new
    @housekeeping = Housekeeping.new
  end

  # GET /housekeepings/1/edit
  def edit
  end

  # POST /housekeepings
  # POST /housekeepings.json
  def create
    #@housekeeping = Housekeeping.new(housekeeping_params)
    #@housekeeping.user = current_user
    @housekeeping = current_user.housekeepings.new(housekeeping_params)
    respond_to do |format|
      if @housekeeping.save
        format.html { redirect_to @housekeeping, notice: I18n.t('housekeeping.created')}
        format.json { render :show, status: :created, location: @housekeeping }
      else
        format.html { render :new }
        format.json { render json: @housekeeping.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /housekeepings/1
  # PATCH/PUT /housekeepings/1.json
  def update
    authorize! :update, @housekeeping
    respond_to do |format|
      if @housekeeping.update(housekeeping_params)
        format.html { redirect_to @housekeeping, notice: I18n.t('housekeeping.updated') }
        format.json { render :show, status: :ok, location: @housekeeping }
      else
        format.html { render :edit }
        format.json { render json: @housekeeping.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /housekeepings/1
  # DELETE /housekeepings/1.json
  def destroy
    authorize! :destroy, @housekeeping
    @housekeeping.destroy
    respond_to do |format|
      format.html { redirect_to housekeepings_url, notice: t('housekeeping.destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_housekeeping
      @housekeeping = Housekeeping.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def housekeeping_params
      params.require(:housekeeping).permit(:name, :descr, :remember, schedule_attributes: Schedulable::ScheduleSupport.param_names)
    end
end
